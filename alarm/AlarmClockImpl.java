
public class AlarmClockImpl implements AlarmClock{
	State
		displayState = new DisplayState(this),
		setHourState = new SetHourState(this),
		setMinState = new SetMinState(this),
		state = displayState;
	int currentHour = 11;
	int currentMin = 32;
	int alarmHour = 6;
	int alarmMin = 15;
	public String readDisplay() {return state.readDisplay();}
	public void mode() {state.mode();}
	public void increase() {state.increase();}
	public void decrease() {state.decrease();}
	
	public static void main ( String [] args ) {
		AlarmClock clock = new AlarmClockImpl();
		System.out.println(clock.readDisplay());
		clock.increase();
		System.out.println(clock.readDisplay());
		clock.decrease();
		System.out.println(clock.readDisplay());
		clock.mode();
		System.out.println(clock.readDisplay());
		clock.increase();clock.increase();clock.increase();
		System.out.println(clock.readDisplay());
		clock.mode();
		clock.decrease();clock.decrease();clock.decrease();clock.decrease();clock.decrease();
		System.out.println(clock.readDisplay());
		clock.mode();
		System.out.println(clock.readDisplay());
		clock.mode();
		System.out.println(clock.readDisplay());
		clock.mode();
		clock.decrease();clock.decrease();clock.decrease();clock.decrease();clock.decrease();
		clock.decrease();clock.decrease();clock.decrease();clock.decrease();clock.decrease();
		clock.decrease();
		System.out.println(clock.readDisplay());
	}
}

abstract class State implements AlarmClock {
	protected AlarmClockImpl alarmclock;
	public State(AlarmClockImpl ac) {alarmclock = ac; }
}

class DisplayState extends State {
	public DisplayState(AlarmClockImpl ac) {super(ac);}
	public String readDisplay() {
		return alarmclock.currentHour+":"+alarmclock.currentMin;
	}
	public void mode() {
		alarmclock.state = alarmclock.setHourState;
	}
	public void increase() {}
	public void decrease() {}
}

class SetHourState extends State {
	public SetHourState(AlarmClockImpl ac) {super(ac);}
	public String readDisplay() {
		return alarmclock.alarmHour+":"+alarmclock.alarmMin;
	}
	public void mode() {
		alarmclock.state = alarmclock.setMinState;
	}
	public void increase() {		
		if (alarmclock.alarmHour == 23) {
			alarmclock.alarmHour = 0;
		} else {
			alarmclock.alarmHour += 1;
		}
	}
	public void decrease() {
		if (alarmclock.alarmHour == 0) {
			alarmclock.alarmHour = 23;
		} else {
			alarmclock.alarmHour -= 1;
		}
	}
}

class SetMinState extends State {
	public SetMinState(AlarmClockImpl ac) {super(ac);}
	public String readDisplay() {
		return alarmclock.alarmHour+":"+alarmclock.alarmMin;
	}
	public void mode() {
		alarmclock.state = alarmclock.displayState;
	}
	public void increase() {
		if (alarmclock.alarmMin == 59) {
			alarmclock.alarmMin = 0;
			alarmclock.alarmHour += 1;
		} else {
			alarmclock.alarmMin += 1;
		}
	}
	public void decrease() {
		if (alarmclock.alarmMin == 0) {
			alarmclock.alarmMin = 59;
			alarmclock.alarmHour -= 1;
		} else {
			alarmclock.alarmMin -= 1;
		}
	}	
}

